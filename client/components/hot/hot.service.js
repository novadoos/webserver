export class hotService
{
  constructor($resource, appConfig) {
    this.$resource = $resource;
    this.appConfig = appConfig;
  }
  crud() {
    return this.$resource(this.appConfig.api + '/opportunities' , null,
      {
        'update': { method:'PUT' }
      });
  }
}
