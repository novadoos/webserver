import { hotService } from './hot.service'

export class hotHandlerService extends hotService
{
  constructor($resource, appConfig) {
    super($resource, appConfig);
  }
  rest(){
    return {
      get: (attr = {}, callback = angular.noop) => {
        return this.crud().get(attr,
          function(data) {
            return callback(data);
          },
          function(err) {
            return callback(err);
          }.bind(this)).$promise;
      }
    };
  }
}

