'use strict';

import angular from 'angular';

export default angular.module('directives.cards', [])
  .component('cards', {
    template: require('./cards.html')
  })
  .name;
