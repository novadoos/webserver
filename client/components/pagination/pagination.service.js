import { hotHandlerService } from '../hot/hot.handler.service';

export class paginationService extends hotHandlerService
{
  constructor($resource, appConfig, $log) {
    super($resource, appConfig);
    this.results = [];
    this.busy = false;
    this.page = 1
  }
  nextPage(){
    if (this.busy) return;
    this.busy = true;

    this.rest().get({page: this.page++})
      .then((result)=> {
        if (!result.opportunities) return;
        for(var i = 0; i < result.opportunities.length; i++) {
          this.results.push(result.opportunities[i]);
        }
        this.busy = false;
      });
  }
}

