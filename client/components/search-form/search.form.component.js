'use strict';

import angular from 'angular';

export class SearchFormComponent {
  /*@ngInject*/
  constructor($state) {
    const $ctrl = this;
    $ctrl.$state = $state;
  }
  searchForm(search){
    const $ctrl = this;
    if(search.$valid){
      $ctrl.$state.go('search', {q: $ctrl.search.search});
    }
  }
}


export default angular.module('fullstackApp.searchForm', [])
  .component('searchForm', {
    template: require('./search-form.html'),
    controller: SearchFormComponent
  })
  .name;
