import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './main.routes';
import { paginationService } from '../../components/pagination/pagination.service';

export class MainController extends paginationService {
  /*@ngInject*/
  constructor($log, $scope, $resource, appConfig) {
    super($resource, appConfig, $log);
    angular.element(document).ready(function () {
      $scope.$emit('list:filtered')
    });
  }
}

export default angular.module('fullstackApp.main', [uiRouter])
  .config(routing)
  .component('main', {
    template: require('./main.html'),
    controller: MainController
  })
  .name;
