'use strict';

import angular from 'angular';
// import ngAnimate from 'angular-animate';
import ngCookies from 'angular-cookies';
import ngResource from 'angular-resource';
import ngSanitize from 'angular-sanitize';

import uiRouter from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';
import ngInfiniteScroll from 'ng-infinite-scroll';
// import ngMessages from 'angular-messages';


import {
  routeConfig
} from './app.config';

import main from './main/main.component';
import search from './search/search.component';
import constants from './app.constants';
import util from '../components/util/util.module';
import cards from '../components/cards/cards.component';
import searchForm from '../components/search-form/search.form.component';

import './app.scss';

angular.module('fullstackApp', [ngCookies, ngResource, ngSanitize, uiRouter, uiBootstrap, ngInfiniteScroll,
    main, search, constants, util, cards, searchForm
  ])
  .config(routeConfig);

angular.element(document)
  .ready(() => {
    angular.bootstrap(document, ['fullstackApp'], {
      strictDi: true
    });
  });
