'use strict';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('search', {
    url: '/search?q',
    template: '<search></search>',
    params: {
      q: {
        value: 'defaultValue'
      }
    }
  });
}
