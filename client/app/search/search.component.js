import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './search.routes';
import { paginationService } from '../../components/pagination/pagination.service';

export class SearchController extends paginationService {
  /*@ngInject*/
  constructor ($stateParams, $state, $log, $resource, appConfig) {
    super($resource, appConfig, $log);
    const vm = this;
    vm.search = {};
    vm.$state = $state;
    vm.$stateParams = $stateParams;
    vm.$log = $log;
    search.search = ($stateParams.q !== 'defaultValue') ? $stateParams.q : '';
  }
  searchForm(search){
    if(search.$valid){
      this.$state.go('search', {q: this.search.search});
    }
  }
}

export default angular.module('fullstackApp.search', [uiRouter])
  .config(routing)
  .component('search', {
    template: require('./search.html'),
    controller: SearchController
  })
  .name;
